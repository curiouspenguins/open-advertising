<!DOCTYPE html>
<html>
  <head>
    <title>JavaScript Random Ad Code Example</title>
    <style>
      /* Style the ad container and text */
      .ad-container {
        background-color: #F5F5F5;
        border: 1px solid #CCCCCC;
        padding: 10px;
        font-family: Arial, sans-serif;
        font-size: 16px;
        line-height: 1.5;
        text-align: center;
        color: #333333;
        position: relative;
      }
      
      .ad-container a {
        color: #333333;
        text-decoration: none;
      }
      
      .ad-container a:hover {
        text-decoration: underline;
      }

      /* Style the "Ads by Driond" text */
      .ad-attribution {
        position: absolute;
        bottom: 5px;
        right: 5px;
        font-size: 12px;
        color: #666666;
      }
    </style>
  </head>
  <body>
    <div id="ad-container" class="ad-container"></div>
    
    <script>
      // Define an array of ad objects
      var ads = [
        {
          image: "https://via.placeholder.com/130x100",
          title: "Example Ad 1",
          text: "Get 20% off your first purchase!",
          link: "https://www.example.com/special-offer"
        },
        {
          image: "https://via.placeholder.com/130x100",
          title: "Example Ad 2",
          text: "Upgrade your wardrobe with our new spring collection",
          link: "https://www.example.com/spring-fashion"
        },
        {
          image: "https://via.placeholder.com/130x100",
          title: "Example Ad 3",
          text: "Find your dream job today",
          link: "https://www.example.com/careers"
        }
      ];

      // Choose a random ad from the array
      var adIndex = Math.floor(Math.random() * ads.length);
      var ad = ads[adIndex];

      // Build the ad HTML
      var adContainer = document.getElementById("ad-container");
      adContainer.innerHTML = '<div><a href="' + ad.link + '"><img src="' + ad.image + '"></a></div>' +
                              '<div><a href="' + ad.link + '"><h2>' + ad.title + '</h2></a>' +
                              '<p>' + ad.text + '</p></div>';

      // Create the ad attribution element
      const adAttribution = document.createElement('div');
      adAttribution.classList.add('ad-attribution');

      // Create the link inside the ad attribution element
      const adLink = document.createElement('a');
      adLink.href = 'https://ads.curiouspenguins.com/';
      adLink.textContent = 'Your Ad Here';

      // Append the link to the ad attribution element
      adAttribution.appendChild(adLink);

      // Append the ad attribution element to the ad container
      adContainer.appendChild(adAttribution);
    </script>
  </body>
</html>
